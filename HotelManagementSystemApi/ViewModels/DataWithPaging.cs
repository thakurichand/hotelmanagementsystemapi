﻿using HotelManagementSystemApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelManagementSystemApi.ViewModels
{
    public class DataWithPaging
    {
        public int TotalPage { get; set; }
        public List<Guests> Guest { get; set; }
        public List<Hotels> Hotel { get; set; }
    }
}