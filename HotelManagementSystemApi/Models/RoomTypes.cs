﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelManagementSystemApi.Models
{
    public class RoomTypes
    {
        [Key]
        public int RoomTypeId { get; set; }
        [Required(ErrorMessage = "Please Enter RoomName")]
        public string RoomName { get; set; }
    }
}