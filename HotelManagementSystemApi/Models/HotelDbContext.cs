﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HotelManagementSystemApi.Models
{
    public class HotelDbContext:DbContext
    {
        public DbSet<Guests> Guest { get; set; }
        public DbSet<Hotels> Hotel { get; set; }
        public DbSet<RoomTypes> RoomType { get; set; }
        public DbSet<Rooms> Room { get; set; }
        public DbSet<Reservations> Reservation { get; set; }
    }
}