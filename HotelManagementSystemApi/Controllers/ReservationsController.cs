﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HotelManagementSystemApi.Models;
using HotelManagementSystemApi.ViewModels;

namespace HotelManagementSystemApi.Controllers
{
    public class ReservationsController : ApiController
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: api/Reservations
        public IQueryable<Reservations> GetReservation()
        {
            return db.Reservation;
        }
        [Route("api/Guests/GetGuests/")]
        public IQueryable<Guests> GetGuests(string searchString)
        {
            IQueryable<Guests> guests;

            if (!String.IsNullOrEmpty(searchString))
            {
                guests = from s in db.Guest where s.FirstName.Contains(searchString) || s.LastName.Contains(searchString) || s.PhoneNo.Contains(searchString) || s.Address.Contains(searchString) select s;
            }
            else
            {
                guests = from s in db.Guest select s;
            }

            return guests;
        }

        [Route("api/Guests/GetPaging")]
        public IHttpActionResult GetPaging(int pageNo = 1)

        {
            int totalPage, totalRecord, pageSize;
            pageSize = 10;
            using (HotelDbContext db = new HotelDbContext())
            {
                totalRecord = db.Guest.Count();
                totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
                var record = (from a in db.Guest
                              orderby a.GuestId
                              select a).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
                DataWithPaging GuestData = new DataWithPaging
                {
                    Guest = record,
                    TotalPage = totalPage
                };
                return Ok(GuestData);
            }
        }


        // GET: api/Reservations/5
        [ResponseType(typeof(Reservations))]
        public IHttpActionResult GetReservations(int id)
        {
            Reservations reservations = db.Reservation.Find(id);
            if (reservations == null)
            {
                return NotFound();
            }

            return Ok(reservations);
        }

        // PUT: api/Reservations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReservations(int id, Reservations reservations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reservations.ReservationId)
            {
                return BadRequest();
            }

            db.Entry(reservations).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReservationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Reservations
        [ResponseType(typeof(Reservations))]
        public IHttpActionResult PostReservations(Reservations reservations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Reservation.Add(reservations);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = reservations.ReservationId }, reservations);
        }

        // DELETE: api/Reservations/5
        [ResponseType(typeof(Reservations))]
        public IHttpActionResult DeleteReservations(int id)
        {
            Reservations reservations = db.Reservation.Find(id);
            if (reservations == null)
            {
                return NotFound();
            }

            db.Reservation.Remove(reservations);
            db.SaveChanges();

            return Ok(reservations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReservationsExists(int id)
        {
            return db.Reservation.Count(e => e.ReservationId == id) > 0;
        }
    }
}