﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HotelManagementSystemApi.Models;
using HotelManagementSystemApi.ViewModels;

namespace HotelManagementSystemApi.Controllers
{
    public class GuestsController : ApiController
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: api/Guests
        public IQueryable<Guests> GetGuest()
        {
            return db.Guest;
        }
        [Route("api/Guests/GetGuests/")]
        public IQueryable<Guests> GetGuests(string searchString)
        {
            IQueryable<Guests> guests;

            if (!String.IsNullOrEmpty(searchString))
            {
                guests = from s in db.Guest where s.FirstName.Contains(searchString) || s.LastName.Contains(searchString) || s.Address.Contains(searchString) || s.PhoneNo.Contains(searchString) select s;
            }
            else
            {
                guests = from s in db.Guest select s;
            }

            return guests;
        }
        [Route("api/Guests/GetPaging")]
        public IHttpActionResult GetPaging(int pageNo = 1)

        {
            int totalPage, totalRecord, pageSize;
            pageSize = 10;
            using (HotelDbContext db = new HotelDbContext())
            {
                totalRecord = db.Hotel.Count();
                totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
                var record = (from a in db.Guest
                              orderby a.GuestId
                              select a).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
                DataWithPaging GuestData = new DataWithPaging
                {
                    Guest = record,
                    TotalPage = totalPage
                };
                return Ok(GuestData);
            }
        }

        // GET: api/Guests/5
        [ResponseType(typeof(Guests))]
        public IHttpActionResult GetGuests(int id)
        {
            Guests guests = db.Guest.Find(id);
            if (guests == null)
            {
                return NotFound();
            }

            return Ok(guests);
        }

        // PUT: api/Guests/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGuests(int id, Guests guests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != guests.GuestId)
            {
                return BadRequest();
            }

            db.Entry(guests).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GuestsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Guests
        [ResponseType(typeof(Guests))]
        public IHttpActionResult PostGuests(Guests guests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Guest.Add(guests);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = guests.GuestId }, guests);
        }

        // DELETE: api/Guests/5
        [ResponseType(typeof(Guests))]
        public IHttpActionResult DeleteGuests(int id)
        {
            Guests guests = db.Guest.Find(id);
            if (guests == null)
            {
                return NotFound();
            }

            db.Guest.Remove(guests);
            db.SaveChanges();

            return Ok(guests);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GuestsExists(int id)
        {
            return db.Guest.Count(e => e.GuestId == id) > 0;
        }
    }
}