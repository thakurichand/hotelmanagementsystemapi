﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HotelManagementSystemApi.Models;
using HotelManagementSystemApi.ViewModels;

namespace HotelManagementSystemApi.Controllers
{
    public class HotelsController : ApiController
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: api/Hotels
        public IQueryable<Hotels> GetHotel()
        {
            return db.Hotel;
        }
        [Route("api/Hotels/GetHotels/")]
        public IQueryable<Hotels> GetHotels(string searchString)
        {
            IQueryable<Hotels> hotels;

            if (!String.IsNullOrEmpty(searchString))
            {
                hotels = from s in db.Hotel where s.HotelName.Contains(searchString) || s.Location.Contains(searchString) || s.PhoneNo.Contains(searchString) select s;
            }
            else
            {
                hotels = from s in db.Hotel select s;
            }

            return hotels;
        }
        [Route("api/Hotels/GetPaging")]
        public IHttpActionResult GetPaging(int pageNo = 1)

        {
            int totalPage, totalRecord, pageSize;
            pageSize = 10;
            using (HotelDbContext db = new HotelDbContext())
            {
                totalRecord = db.Hotel.Count();
                totalPage = (totalRecord / pageSize) + ((totalRecord % pageSize) > 0 ? 1 : 0);
                var record = (from a in db.Hotel
                              orderby a.HotelId
                              select a).Skip((pageNo - 1) * pageSize).Take(pageSize).ToList();
                DataWithPaging HotelData = new DataWithPaging
                {
                    Hotel = record,
                    TotalPage = totalPage
                };
                return Ok(HotelData);
            }
        }

        // GET: api/Hotels/5
        [ResponseType(typeof(Hotels))]
        public IHttpActionResult GetHotels(int id)
        {
            Hotels hotels = db.Hotel.Find(id);
            if (hotels == null)
            {
                return NotFound();
            }

            return Ok(hotels);
        }

        // PUT: api/Hotels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHotels(int id, Hotels hotels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hotels.HotelId)
            {
                return BadRequest();
            }

            db.Entry(hotels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HotelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hotels
        [ResponseType(typeof(Hotels))]
        public IHttpActionResult PostHotels(Hotels hotels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Hotel.Add(hotels);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = hotels.HotelId }, hotels);
        }

        // DELETE: api/Hotels/5
        [ResponseType(typeof(Hotels))]
        public IHttpActionResult DeleteHotels(int id)
        {
            Hotels hotels = db.Hotel.Find(id);
            if (hotels == null)
            {
                return NotFound();
            }

            db.Hotel.Remove(hotels);
            db.SaveChanges();

            return Ok(hotels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HotelsExists(int id)
        {
            return db.Hotel.Count(e => e.HotelId == id) > 0;
        }
    }
}