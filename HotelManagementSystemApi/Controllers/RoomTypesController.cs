﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using HotelManagementSystemApi.Models;

namespace HotelManagementSystemApi.Controllers
{
    public class RoomTypesController : ApiController
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: api/RoomTypes
        public IQueryable<RoomTypes> GetRoomType()
        {
            return db.RoomType;
        }

        // GET: api/RoomTypes/5
        [ResponseType(typeof(RoomTypes))]
        public IHttpActionResult GetRoomTypes(int id)
        {
            RoomTypes roomTypes = db.RoomType.Find(id);
            if (roomTypes == null)
            {
                return NotFound();
            }

            return Ok(roomTypes);
        }

        // PUT: api/RoomTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRoomTypes(int id, RoomTypes roomTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != roomTypes.RoomTypeId)
            {
                return BadRequest();
            }

            db.Entry(roomTypes).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoomTypesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RoomTypes
        [ResponseType(typeof(RoomTypes))]
        public IHttpActionResult PostRoomTypes(RoomTypes roomTypes)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.RoomType.Add(roomTypes);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = roomTypes.RoomTypeId }, roomTypes);
        }

        // DELETE: api/RoomTypes/5
        [ResponseType(typeof(RoomTypes))]
        public IHttpActionResult DeleteRoomTypes(int id)
        {
            RoomTypes roomTypes = db.RoomType.Find(id);
            if (roomTypes == null)
            {
                return NotFound();
            }

            db.RoomType.Remove(roomTypes);
            db.SaveChanges();

            return Ok(roomTypes);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoomTypesExists(int id)
        {
            return db.RoomType.Count(e => e.RoomTypeId == id) > 0;
        }
    }
}